"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gTotalPages = 0;
    var gPage = 0;
    var gPageSize = 10;
    var gId = "";
    var gKeyword = "";
    var gSortBy = "0";
    var gTotalSales = "-1";
    var gStatus = "All"
    const gNameCol = ["action", "id", "userPhoto", "firstName", "lastName", "username", "totalOrders", "totalSales", "roles", "deleted", "activated"];
    const gACTION_COL = 0;
    const gID_COL = 1;
    const gUSER_PHOTO_COL = 2;
    const gFIRST_NAME_COL = 3;
    const gLAST_NAME_COL = 4;
    const gUSERNAME_COL = 5;   
    const gTOTAL_ORDERS_COL = 6;
    const gTOTAL_SALES_COL = 7;
    const gROLES_COL = 8;
    const gENABLED_COL = 9;
    const gACTIVATED_COL = 10;
    var gDataTable = $("#table-users").DataTable({
        columns: [
            { data: gNameCol[gACTION_COL] },
            { data: gNameCol[gID_COL] },
            { data: gNameCol[gUSER_PHOTO_COL] },
            { data: gNameCol[gFIRST_NAME_COL] },
            { data: gNameCol[gLAST_NAME_COL] },
            { data: gNameCol[gUSERNAME_COL] },
            { data: gNameCol[gTOTAL_ORDERS_COL] },
            { data: gNameCol[gTOTAL_SALES_COL] },
            { data: gNameCol[gROLES_COL] },
            { data: gNameCol[gENABLED_COL] },
            { data: gNameCol[gACTIVATED_COL] }
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: "<a href='javascript:void(0)' class='dropdown'><a href='javascript:void(0)' class='dropdown-toggle text-dark me-3' data-bs-toggle='dropdown' aria-expanded='false'><i class='fa-solid fa-ellipsis'></i></a><ul class='dropdown-menu'><li><a class='dropdown-item item-edit' href='javascript:void(0)'>Edit</a></li></ul></a><a href='javascript:void(0)' class='item-detail' title='Detail'><i class='text-info fa-solid fa-circle-info'></i></a>",
                className: "text-nowrap"
            },
            {
                targets: gUSER_PHOTO_COL,
                render: function (data, type) {
                    if (data != null) {
                        return "<img src='" + gBASE_URL + "user-photos/" + data.id + "' class='rounded-circle' style='height: 30px; object-fit: contain;'/>"
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gFIRST_NAME_COL,
                className: "text-nowrap"
            },
            {
                targets: gLAST_NAME_COL,
                className: "text-nowrap"
            },
            {
                targets: gTOTAL_ORDERS_COL,
                render: function (data, type) {
                    if (data > 0) {
                        return "<a  href='javascript:void(0)' class='status-btn primary-btn item-total-orders'>" + data + "</a>"
                    }
                    else {
                        return "";
                    }
                }
            },
            {
                targets: gTOTAL_SALES_COL,
                render: function (data, type) {
                    return data.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }
            },
            {
                targets: gROLES_COL,
                render: function (data, type) {
                    var vText = "";
                    if (data != null) {
                        for (var bI = 0; bI < data.length - 1; bI++) {
                            vText += data[bI].roleKey + ", ";
                        }
                        vText += data[data.length - 1].roleKey;
                    }
                    return vText;
                },
                className: "text-nowrap"
            },
            {
                targets: gENABLED_COL,
                render: function (data, type) {
                    if (data != null) {
                        return stringEnabled(data);
                    }
                    return "";
                },
                className: "text-nowrap"
            },
            {
                targets: gACTIVATED_COL,
                render: function (data, type) {
                    if (data != null) {
                        return stringActivated(data);
                    }
                    return "";
                },
                className: "text-nowrap"
            }
        ],
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        scrollY: 400,
        scrollX: true,
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện khi nhập vào ô tìm kiếm
    $("#inp-search").on("input", function () {
        onSearchInput();
    });

    //Gán sự kiện cho nút add
    $("#add-user").on("click", function () {
        onAddUserClick();
    });

    //Gán sự kiện cho nút Filter
    $("#filter-user").on("click", onBtnFilterClick);

    //Gán sự kiện cho nút Filter trên modal filter
    $("#btn-filter").on("click", onBtnModalFilterClick);

    //Gán sự kiện cho nút Reset trên modal filter
    $("#btn-reset-filter").on("click", onBtnModalResetFilterClick);

    //Gán sự kiện cho nút Detail
    $("#table-users tbody").on("click", ".item-detail", function () {
        onItemDetailClick(this);
    });

    //Gán sự kiện cho nút Total Orders
    $("#table-users tbody").on("click", ".item-total-orders", function () {
        onItemTotalOrdersClick(this);
    });

    //Gán sự kiện cho nút Edit
    $("#table-users tbody").on("click", ".item-edit", function () {
        onItemEditClick(this);
    });

    //Gán sự kiện cho nút Edit trên modal Detail
    $("#btn-edit").on("click", onModalDetailEditClick);

    //Gán sự kiện cho nút Enable / Disable trên modal Detail
    $("#btn-enable-disable").on("click", onModalDetailEnableDisableClick);

    //Gán sự kiện cho nút Enable / Disable trên modal
    $("#btn-enable-disable-confirm").on("click", onBtnEnableDisableConfirmClick);

    //Gán sự kiện cho các nút phân trang
    $("#number-page").on("keypress", function (event) {
        if (event.which == 13) {
            onNumberPageEnter();
        }
    });
    $("#sel-page-size").on("change", onChangePageSize);
    $("#first-page").on("click", onFirstPageClick);
    $("#previous-page").on("click", onPreviousPageClick);
    $("#next-page").on("click", onNextPageClick);
    $("#last-page").on("click", onLastPageClick);

    //Điều chỉnh lại các cột table khi thay đổi kích thước window
    $(window).resize(function () {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        loadUserDataToTable();
    }

    // Hàm xử lý khi nhập vào ô tìm kiếm
    function onSearchInput() {
        "use strict";
        gKeyword = $("#inp-search").val();
        gPage = 0;
        loadUserDataToTable();

    }

    // Hàm xử lý khi click nút add
    function onAddUserClick() {
        "use strict";
        window.location.href = "add-user.html";
    }

    // Hàm xử lý khi click nút Filter
    function onBtnFilterClick() {
        "use strict";
        $("#sel-customerType").val(gTotalSales);
        $("#modal-filter").modal("show");
    }

    // Hàm xử lý khi click nút Filter trên modal filter
    function onBtnModalFilterClick() {
        "use strict";
        gTotalSales = $("#sel-customerType").val();
        if (gTotalSales == "-1") {
            gStatus = "All";
        } else {
            gStatus = "Completed";
        }
        gPage = 0;
        loadUserDataToTable();
        $("#modal-filter").modal("hide");
    }

    // Hàm xử lý khi click nút Reset trên modal filter
    function onBtnModalResetFilterClick() {
        "use strict";
        gTotalSales = "-1";
        gStatus = "All";
        loadUserDataToTable();
        $("#modal-filter").modal("hide");
    }

    // Hàm xử lý khi click nút Detail
    function onItemDetailClick(paramBtnDetail) {
        "use strict";
        gId = gDataTable.row(paramBtnDetail.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "users/" + gId,
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    loadDetailData(res);
                    $("#modal-detail").modal("show");
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    // Hàm xử lý khi click nút Total Orders
    function onItemTotalOrdersClick(paramBtnTotalOrders) {
        "use strict";
        var vCustomerId = gDataTable.row(paramBtnTotalOrders.closest("tr")).data().id;
        sessionStorage.setItem("customerId", vCustomerId);
        window.location.href = "orders.html";
    }

    // Hàm xử lý khi click nút edit
    function onItemEditClick(paramBtnDetail) {
        "use strict";
        gId = gDataTable.row(paramBtnDetail.closest("tr")).data().id;
        window.location.href = "edit-user.html?id=" + gId;
    }

    // Hàm xử lý khi click nút Edit trên Modal Detail
    function onModalDetailEditClick() {
        "use strict";
        $("#modal-detail").modal("hide");
        window.location.href = "edit-user.html?id=" + gId;
    }

    // Hàm xử lý khi click nút Enable / Disable trên Modal Detail
    function onModalDetailEnableDisableClick() {
        "use strict";
        $("#modal-detail").modal("hide");
        $("#modal-enable-disable").modal("show");
    }

    // Hàm xử lý khi click nút Enable / Disable trên modal
    function onBtnEnableDisableConfirmClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "users/" + gId,
                method: "PATCH",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    $("#modal-enable-disable").modal("hide");
                    loadUserDataToTable();
                }, error: function (err) {
                    $("#modal-enable-disable").modal("hide");
                    $("#modal-error").modal("show");
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    function onNumberPageEnter() {
        var vPage = $("#number-page").val();
        if (vPage < 1) {
            gPage = 0;
            loadUserDataToTable();
        } else if (vPage > gTotalPages && gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadUserDataToTable();
        } else {
            gPage = vPage - 1;
            loadUserDataToTable();
        }
    }

    function onChangePageSize() {
        gPage = 0;
        gPageSize = $("#sel-page-size").val();
        loadUserDataToTable();
    }

    //Hàm xử lý khi ấn nút Trang đầu
    function onFirstPageClick() {
        if (gTotalPages > 0) {
            gPage = 0;
            loadUserDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang trước
    function onPreviousPageClick() {
        if (gTotalPages > 0 && gPage > 0) {
            gPage--;
            loadUserDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang sau
    function onNextPageClick() {
        if (gTotalPages > 0 && gPage < gTotalPages - 1) {
            gPage++;
            loadUserDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang cuối
    function onLastPageClick() {
        if (gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadUserDataToTable();
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm load dữ liệu User vào bảng
    function loadUserDataToTable() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vUrl = "";
            if(gTotalSales == "-1") {
                vUrl = "users";
            } else {
                vUrl = "users/customer-type"
            }
            $.ajax({
                url: gBASE_URL + vUrl + "?keyword=" + gKeyword + "&status=" + gStatus + "&totalSales=" + gTotalSales + "&page=" + gPage + "&size=" + gPageSize,
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    gDataTable.clear();
                    gDataTable.rows.add(res.content);
                    gDataTable.draw();
                    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
                    gTotalPages = res.totalPages;
                    $("#number-page").val(gPage + 1);
                    $("#element-first").html(gPage * gPageSize + (res.numberOfElements !== 0 ? 1 : 0));
                    $("#element-last").html(gPage * gPageSize + res.numberOfElements);
                    $("#total-elements").html(res.totalElements);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý hiển thị chi tiết
    function loadDetailData(paramData) {
        "use strict";
        $("#p-id").html(paramData.id);
        if (paramData.userPhotoId != null && paramData.userPhotoId != "") {
            $("#img-userPhoto").html("<img src='" + gBASE_URL + "user-photos/" + paramData.userPhotoId + "' class='d-block w-100' style='height: 300px; object-fit: contain;' alt='...'>");
        } else {
            $("#img-userPhoto").html("");
        }
        $("#p-firstName").html(paramData.firstName);
        $("#p-lastName").html(paramData.lastName);
        $("#p-username").html(paramData.username);
        var vRoles = paramData.roles;
        if (vRoles != null) {
            var vText = "";
            for (var bI = 0; bI < vRoles.length - 1; bI++) {
                vText += vRoles[bI].roleKey + ", ";
            }
            vText += vRoles[vRoles.length - 1].roleKey;
            $("#p-roles").html(vText);
        } else {
            $("#p-roles").html("");
        }
        $("#p-deleted").html(stringEnabled(paramData.deleted));
        $("#p-activated").html(stringActivated(paramData.activated));
        $("#p-createdAt").html(paramData.createdAt);
        $("#p-updatedAt").html(paramData.updatedAt);
        $("#p-createdBy").html(paramData.createdBy);
        $("#p-updatedBy").html(paramData.updatedBy);
        if (paramData.deleted) {
            $("#btn-enable-disable").html("Enable");
            $("#modal-enable-disable .enable-user").prop("hidden", false);
            $("#modal-enable-disable .disable-user").prop("hidden", true);
            $("#btn-enable-disable-confirm").html("Enable");
        } else {
            $("#btn-enable-disable").html("Disable");
            $("#modal-enable-disable .enable-user").prop("hidden", true);
            $("#modal-enable-disable .disable-user").prop("hidden", false);
            $("#btn-enable-disable-confirm").html("Disable");
        }
    }

    function stringEnabled(paramCheck) {
        "use strict";
        if (paramCheck) {
            return "<span class='text-danger'>No</span>";
        }
        return "Yes";
    }

    function stringActivated(paramCheck) {
        "use strict";
        if (!paramCheck) {
            return "<span class='text-danger'>No</span>";
        }
        return "Yes";
    }
});