"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gTotalPages = 0;
    var gPage = 0;
    var gPageSize = 10;
    var gId = "";
    var gKeyword = "";
    const gNameCol = ["action", "id", "firstName", "lastName", "extension", "email", "office", "reportTo", "jobTitle"];
    const gACTION_COL = 0;
    const gID_COL = 1;
    const gFIRST_NAME_COL = 2;
    const gLAST_NAME_COL = 3;
    const gEXTENSION_COL = 4;
    const gEMAIL_COL = 5;
    const gOFFICE_COL = 6;
    const gREPORT_TO_COL = 7;
    const gJOB_TITLE_COL = 8;
    var gDataTable = $("#table-employees").DataTable({
        columns: [
            { data: gNameCol[gACTION_COL] },
            { data: gNameCol[gID_COL] },
            { data: gNameCol[gFIRST_NAME_COL] },
            { data: gNameCol[gLAST_NAME_COL] },
            { data: gNameCol[gEXTENSION_COL] },
            { data: gNameCol[gEMAIL_COL] },
            { data: gNameCol[gOFFICE_COL] },
            { data: gNameCol[gREPORT_TO_COL] },
            { data: gNameCol[gJOB_TITLE_COL] }
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: "<a href='javascript:void(0)' class='dropdown'><a href='javascript:void(0)' class='dropdown-toggle text-dark me-3' data-bs-toggle='dropdown' aria-expanded='false'><i class='fa-solid fa-ellipsis'></i></a><ul class='dropdown-menu'><li><a class='dropdown-item item-edit' href='javascript:void(0)'>Edit</a></li><li><a class='dropdown-item item-delete' href='javascript:void(0)'>Delete</a></li></ul></a><a href='javascript:void(0)' class='item-detail' title='Detail'><i class='text-info fa-solid fa-circle-info'></i></a>",
                className: "text-nowrap"
            },
            {
                targets: gOFFICE_COL,
                render: function (data, type) {
                    if (data != null) {
                        return data.id;
                    }
                    return "";
                }
            }
        ],
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        scrollY: 400,
        scrollX: true,
    });

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện khi nhập vào ô tìm kiếm
    $("#inp-search").on("input", function () {
        onSearchInput();
    });

    //Gán sự kiện cho nút add
    $("#add-employee").on("click", function () {
        onAddEmployeeClick();
    });

    //Gán sự kiện cho nút Detail
    $("#table-employees tbody").on("click", ".item-detail", function () {
        onItemDetailClick(this);
    });

    //Gán sự kiện cho nút Edit
    $("#table-employees tbody").on("click", ".item-edit", function () {
        onItemEditClick(this);
    });

    //Gán sự kiện cho nút Delete
    $("#table-employees tbody").on("click", ".item-delete", function () {
        onItemDeleteClick(this);
    });

    //Gán sự kiện cho nút Edit trên modal Detail
    $("#btn-edit").on("click", onModalDetailEditClick);

    //Gán sự kiện cho nút Delete trên modal Detail
    $("#btn-delete").on("click", onModalDetailDeleteClick);

    //Gán sự kiện cho nút Delete trên modal
    $("#btn-delete-confirm").on("click", onBtnDeleteConfirmClick);

    //Gán sự kiện cho các nút phân trang
    $("#number-page").on("keypress", function (event) {
        if (event.which == 13) {
            onNumberPageEnter();
        }
    });
    $("#sel-page-size").on("change", onChangePageSize);
    $("#first-page").on("click", onFirstPageClick);
    $("#previous-page").on("click", onPreviousPageClick);
    $("#next-page").on("click", onNextPageClick);
    $("#last-page").on("click", onLastPageClick);

    //Điều chỉnh lại các cột table khi thay đổi kích thước window
    $(window).resize(function () {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        loadEmployeeDataToTable();
    }

    // Hàm xử lý khi nhập vào ô tìm kiếm
    function onSearchInput() {
        "use strict";
        gKeyword = $("#inp-search").val();
        gPage = 0;
        loadEmployeeDataToTable();

    }

    // Hàm xử lý khi click nút add
    function onAddEmployeeClick() {
        "use strict";
        window.location.href = "add-employee.html";
    }

    // Hàm xử lý khi click nút Detail
    function onItemDetailClick(paramBtnDetail) {
        "use strict";
        gId = gDataTable.row(paramBtnDetail.closest("tr")).data().id;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "employees/" + gId,
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    loadDetailData(res);
                    $("#modal-detail").modal("show");
                }, error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    // Hàm xử lý khi click nút edit
    function onItemEditClick(paramBtnDetail) {
        "use strict";
        gId = gDataTable.row(paramBtnDetail.closest("tr")).data().id;
        window.location.href = "edit-employee.html?id=" + gId;
    }

    // Hàm xử lý khi click nút Delete
    function onItemDeleteClick(paramBtnDetail) {
        "use strict";
        gId = gDataTable.row(paramBtnDetail.closest("tr")).data().id;
        $("#modal-delete").modal("show");
    }

    // Hàm xử lý khi click nút Edit trên Modal Detail
    function onModalDetailEditClick() {
        "use strict";
        $("#modal-detail").modal("hide");
        window.location.href = "edit-employee.html?id=" + gId;
    }

    // Hàm xử lý khi click nút Delete trên Modal Detail
    function onModalDetailDeleteClick() {
        "use strict";
        $("#modal-detail").modal("hide");
        $("#modal-delete").modal("show");
    }

    // Hàm xử lý khi click nút delete trên modal
    function onBtnDeleteConfirmClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "employees/" + gId,
                method: "DELETE",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    $("#modal-delete").modal("hide");
                    loadEmployeeDataToTable();
                }, error: function (err) {
                    $("#modal-delete").modal("hide");
                    $("#modal-error").modal("show");
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    function onNumberPageEnter() {
        var vPage = $("#number-page").val();
        if (vPage < 1) {
            gPage = 0;
            loadEmployeeDataToTable();
        } else if (vPage > gTotalPages && gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadEmployeeDataToTable();
        } else {
            gPage = vPage - 1;
            loadEmployeeDataToTable();
        }
    }

    function onChangePageSize() {
        gPage = 0;
        gPageSize = $("#sel-page-size").val();
        loadEmployeeDataToTable();
    }

    //Hàm xử lý khi ấn nút Trang đầu
    function onFirstPageClick() {
        if (gTotalPages > 0) {
            gPage = 0;
            loadEmployeeDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang trước
    function onPreviousPageClick() {
        if (gTotalPages > 0 && gPage > 0) {
            gPage--;
            loadEmployeeDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang sau
    function onNextPageClick() {
        if (gTotalPages > 0 && gPage < gTotalPages - 1) {
            gPage++;
            loadEmployeeDataToTable();
        }
    }

    //Hàm xử lý khi ấn nút Trang cuối
    function onLastPageClick() {
        if (gTotalPages > 0) {
            gPage = gTotalPages - 1;
            loadEmployeeDataToTable();
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm load dữ liệu Employee vào bảng
    function loadEmployeeDataToTable() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "employees?keyword=" + gKeyword + "&page=" + gPage + "&size=" + gPageSize,
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                success: function (res) {
                    gDataTable.clear();
                    gDataTable.rows.add(res.content);
                    gDataTable.draw();
                    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
                    gTotalPages = res.totalPages;
                    $("#number-page").val(gPage + 1);
                    $("#element-first").html(gPage * gPageSize + (res.numberOfElements !== 0 ? 1 : 0));
                    $("#element-last").html(gPage * gPageSize + res.numberOfElements);
                    $("#total-elements").html(res.totalElements);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý hiển thị chi tiết
    function loadDetailData(paramData) {
        "use strict";
        $("#p-id").html(paramData.id);
        $("#p-firstName").html(paramData.firstName);
        $("#p-lastName").html(paramData.lastName);
        $("#p-extension").html(paramData.extension);
        $("#p-email").html(paramData.email);
        $("#p-office").html(paramData.office.id + " - " + paramData.office.phone + " - " + paramData.office.addressLine);
        $("#p-reportTo").html(paramData.reportTo);
        $("#p-jobTitle").html(paramData.jobTitle);
    }
});