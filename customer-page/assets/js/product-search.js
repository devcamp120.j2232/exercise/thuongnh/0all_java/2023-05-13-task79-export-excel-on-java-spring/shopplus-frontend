"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
//Gán sự kiện cho nút Search
$("#btn-search").on("click", onBtnSearchClick);
$("#inp-search").on("keypress", function (event) {
    if (event.which == 13) {
        onBtnSearchClick();
    }
});

//Gán sự kiện click Product Brand
$("#sub-category").on("click", ".li-product-brand", function () {
    onProductBrandClick($(this));
});

//Gán sự kiện click Product Line
$("#sub-category").on("click", ".li-product-line", function () {
    onProductLineClick($(this));
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
$.ajax({
    url: gBASE_URL + "product-brands/all",
    method: "GET",
    async: false,
    success: function (res) {
        loadDataToCategoryMenu(res);
    },
    error: function (err) {
        console.log(err.responseText);
    }
});

//Hàm xử lý khi ấn nút Search
function onBtnSearchClick() {
    "use strict";
    var vKeyword = $("#inp-search").val().trim();
    if (vKeyword != "") {
        sessionStorage.setItem("searchKeyword", vKeyword);
        window.location.href = "products.html";
    }
}

//Hàm xử lý khi click Product Brand
function onProductBrandClick(paramElement) {
    "use strict";
    var vProductBrandId = paramElement[0].dataset.productBrandId;
    sessionStorage.setItem("productBrandId", vProductBrandId);
    window.location.href = "products.html";
}

//Hàm xử lý khi click Product Line
function onProductLineClick(paramElement) {
    "use strict";
    var vProductBrandId = paramElement[0].dataset.productBrandId;
    var vProductLineId = paramElement[0].dataset.productLineId;
    sessionStorage.setItem("productBrandId", vProductBrandId);
    sessionStorage.setItem("productLineId", vProductLineId);
    window.location.href = "products.html";
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm load dữ liệu vào Category Menu
function loadDataToCategoryMenu(paramData) {
    "use strict";
    for (var bI = 0; bI < paramData.length; bI++) {
        var bId = paramData[bI].id;
        var bName = paramData[bI].name;
        var vText = "";
        vText += "<li class='li-product-brand' data-product-brand-id='" + bId + "'><a href='javascript:void(0)'>" + bName + " <i class='fa-solid fa-chevron-right'></i></a>"
            + "<ul class='inner-sub-category'>" + getProductLineByProductBrand(bId) + "</ul>"
            + "</li>";
        $("#sub-category").append(vText);
    }
}

//Hàm lấy dữ liệu Product Line
function getProductLineByProductBrand(paramId) {
    "use strict";
    var vText = "";
    $.ajax({
        url: "http://localhost:8080/product-brands/" + paramId + "/product-lines",
        method: "GET",
        async: false,
        success: function (res) {
            for (var bI = 0; bI < res.length; bI++) {
                var bId = res[bI].id;
                var bName = res[bI].fullName;
                vText += "<li data-product-brand-id='" + paramId + "' data-product-line-id='" + bId + "' class='li-product-line'><a href='javascript:void(0)'>" + bName + "</a></li>";
            }
        },
        error: function (err) {
            console.log(err.responseText);
        }
    });
    return vText;
}
