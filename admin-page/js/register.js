$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Kiểm tra dữ liệu nhập vào trên form register
    $("#form-register").validate({
        submitHandler: function () {
            registerFunction();
        },
        rules: {
            inp_firstName: {
                required: true,
            },
            inp_lastName: {
                required: true,
            },
            inp_email: {
                required: true,
                email: true
            },
            inp_password: {
                required: true,
                minlength: 6
            },
            inp_password_confirm: {
                required: true,
                minlength: 6,
                equalTo: "#inp-password"
            }
        },
        messages: {
            inp_firstName: {
                required: "Please provide a First Name.",
            },
            inp_lastName: {
                required: "Please provide a Last Name.",
            },
            inp_email: {
                required: "Please provide an Email.",
                email: "Please Enter a valid Email."
            },
            inp_password: {
                required: "Please provide a Password.",
                minlength: "Password must has at least 6 characters."
            },
            inp_password_confirm: {
                required: "Please provide a Password.",
                minlength: "Password must has at least 6 characters.",
                equalTo: "Passwords do not match."
            }
        },
        errorElement: 'span',
        errorClass: 'invalid-feedback',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        checkCookie();
    }

    //Hàm xử lý Đăng ký
    function registerFunction() {
        "use strict";
        var vRegisterData = {
            firstName: $("#inp-firstName").val().trim(),
            lastName: $("#inp-lastName").val().trim(),
            username: $("#inp-email").val().trim(),
            password: $("#inp-password").val()
        }
        var vIsCheck = validateData(vRegisterData);
        if (vIsCheck) {
            $.ajax({
                url: gBASE_URL + "auth/register",
                method: "POST",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(vRegisterData),
                success: function (res) {
                    sessionStorage.setItem("registerSuccess", 1);
                    window.location.href = "login.html";
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm kiểm tra cookie
    function checkCookie() {
        "use strict";
        const token = getCookie("token");
        if (token) {
            window.location.href = "index.html";
        }
    }

    //Hàm kiểm tra dữ liệu nhập vào
    function validateData(paramRegisterData) {
        if (checkUsername(paramRegisterData.username)) {
            toastr.warning("Email already exists!");
            return false;
        }
        return true;
    }

    //Hàm kiểm tra username
    function checkUsername(paramUsername) {
        "use strict";
        var vIsCheck = true;
        $.ajax({
            url: gBASE_URL + "users/username/" + paramUsername + "/exists",
            method: "GET",
            async: false,
            success: function (res) {
                vIsCheck = res;
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
        return vIsCheck;
    }
});