$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gId = "";
    var gName = "";

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading();

    //Gán sự kiện click cho nút Save ProcessorCollection
    Array.from($("#form-edit")).forEach(form => {
        form.addEventListener("submit", event => {
            event.preventDefault();
            event.stopPropagation();
            if (form.checkValidity()) {
                onBtnUpdateClick();
            }
            form.classList.add("was-validated");
        }, false);
    });

    //Gán sự kiện cho nút Cancel
    $("#btn-cancel").on("click", function () {
        window.history.back();
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        const vUrlParams = new URLSearchParams(window.location.search);
        gId = vUrlParams.get("id");
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "processor-brands/all",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    loadDataToSelectProcessorBrand(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });

            if (gId != "" && gId != null) {
                $.ajax({
                    url: gBASE_URL + "processor-collections/" + gId,
                    method: "GET",
                    headers: {
                        Authorization: "Bearer " + token
                    },
                    success: function (res) {
                        loadEditData(res);
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    //Hàm xử lý khi ấn nút Update
    function onBtnUpdateClick() {
        "use strict";
        var token = getCookie("token");
        if (token) {
            var vProcessorCollection = {
                processorBrand: "",
                name: "",
                description: "",
            }
            //Lấy dữ liệu
            getProcessorCollectionData(vProcessorCollection);
            //Kiểm tra dữ liệu
            var vIsCheck = gName == vProcessorCollection.name ? true : !checkName(vProcessorCollection.name);
            if (vIsCheck) {
                $.ajax({
                    url: gBASE_URL + "processor-collections/" + gId,
                    method: "PUT",
                    async: false,
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify(vProcessorCollection),
                    success: function (res) {
                        window.location.href = "processor-collections.html";
                    },
                    error: function (err) {
                        console.log(err.responseText);
                    }
                });
            } else {
                toastr.warning("Name already exists!");
            }
        } else {
            toastr.error("You are not logged in!");
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm load dữ liệu vào Select Processor Brand
    function loadDataToSelectProcessorBrand(paramData) {
        "use strict";
        for (var bI = 0; bI < paramData.length; bI++) {
            var bId = paramData[bI].id;
            var bName = paramData[bI].name;
            $("#sel-processorBrand").append($("<option>").val(bId).text(bName));
        }
    }

    //Hàm xử lý hiển thị dữ liệu Processor Collection
    function loadEditData(paramData) {
        "use strict";
        if (paramData.processorBrand != null) {
            $("#sel-processorBrand").val(paramData.processorBrand.id).trigger("change");
        } else {
            $("#sel-processorBrand").val("").trigger("change");
        }
        gName = paramData.name;
        $("#inp-name").val(gName);
        $("#inp-description").val(paramData.description);
    }

    //Hàm lấy dữ liệu Processor Collection
    function getProcessorCollectionData(paramData) {
        paramData.processorBrand = {
            id: $("#sel-processorBrand").val().trim()
        }
        paramData.name = $("#inp-name").val().trim();
        paramData.description = $("#inp-description").val().trim();
    }

    //Hàm kiểm tra name
    function checkName(paramName) {
        "use strict";
        var vIsCheck = true;
        var token = getCookie("token");
        if (token) {
            $.ajax({
                url: gBASE_URL + "processor-collections/name/" + paramName + "/exists",
                method: "GET",
                headers: {
                    Authorization: "Bearer " + token
                },
                async: false,
                success: function (res) {
                    vIsCheck = res;
                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        } else {
            toastr.error("You are not logged in!");
        }
        return vIsCheck;
    }
});