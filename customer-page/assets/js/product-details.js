"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gId = "";

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
onPageLoading();

//Gán sự kiện cho nút View more
$("#view-more").on("click", function () {
    $("#modal-view-more").modal("show");
});

//Gán sự kiện cho nút Add to Cart
$("#btn-add-to-cart").on("click", onBtnAddToCart);

//Gán sự kiện cho nút To Wishlist
$("#btn-to-wishlist").on("click", onBtnToWishList);

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    "use strict";
    const vUrlParams = new URLSearchParams(window.location.search);
    gId = vUrlParams.get("id");
    if (gId != "" && gId != null) {
        $.ajax({
            url: gBASE_URL + "products/" + gId,
            method: "GET",
            async: false,
            success: function (res) {
                loadDetailData(res);
                loadViewMoreData(res);
                if (res.deleted == true) {
                    //$("#p-message").html("<p><b class='text-danger mb-2'>This product has been discontinued !</b></p>");
                }
            },
            error: function (err) {
                window.location.href = "404.html";
            }
        });
    } else {
        window.location.href = "index.html";
    }

    var vOrderDetails = getCookie("order_details") ? JSON.parse(getCookie("order_details")) : "";
    if (vOrderDetails != null && vOrderDetails != "") {
        vOrderDetails.forEach(element => {
            if (element.productId == gId) {
                $("#lb-add-to-cart").prop("hidden", false);
                $("#sel-quantity").val(element.quantityOrder);
            }
        });
    }
}

//Hàm xử lý khi ấn nút Add to Cart
function onBtnAddToCart() {
    "use strict";
    var vOrderDetails = getCookie("order_details") ? JSON.parse(getCookie("order_details")) : "";
    if (vOrderDetails == null || vOrderDetails == "") {
        vOrderDetails = [];
    }
    if (gId != "" && gId != null) {
        var vIsElementExists = false;
        vOrderDetails.forEach((element, index) => {
            if (element.productId == gId) {
                vIsElementExists = true;
                vOrderDetails[index] = {
                    productId: gId,
                    quantityOrder: $("#sel-quantity").val()
                }
                toastr.success("Added to cart");
            }
        });
        if (!vIsElementExists) {
            var vOrderDetail = {
                productId: gId,
                quantityOrder: $("#sel-quantity").val()
            }
            vOrderDetails.push(vOrderDetail);
            toastr.success("Added to cart");
        }

        $("#lb-add-to-cart").prop("hidden", false);

        var vOrderDetailsJson = JSON.stringify(vOrderDetails);
        setCookie("order_details", vOrderDetailsJson, 1, true);

        var vTotalItems = 0;
        vOrderDetails.forEach(element => {
            vTotalItems += parseInt(element.quantityOrder);
        });
        $("#total-cart-items-1").html(vTotalItems);
    }
}

//Hàm xử lý khi ấn nút To Wishlist
function onBtnToWishList() {
    "use strict";
    var vListProducts = getCookie("wishlist") ? JSON.parse(getCookie("wishlist")) : "";
    if (vListProducts == null || vListProducts == "") {
        vListProducts = [];
    }
    if (gId != "" && gId != null) {
        var vCount = 0;
        vListProducts.forEach((element, index) => {
            if (element == gId) {
                vCount++;
                vListProducts[index] = gId;
                toastr.success("Added to wish list");
            }
        });
        if (vCount == 0) {
            vListProducts.push(gId);
            toastr.success("Added to wish list");
        }
        var vOrderDetailsJson = JSON.stringify(vListProducts);
        setCookie("wishlist", vOrderDetailsJson, 10, true);

        var vTotalItems = vListProducts.length;
        $("#total-wishlist-items-1").html(vTotalItems);
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm xử lý hiển thị chi tiết Product
function loadDetailData(paramData) {
    "use strict";
    if (paramData.deleted == true || paramData.quantityInStock < 5) {
        $("#disable-add-to-cart").prop("hidden", true);
        $("#disable-quantity").prop("hidden", true);
    }

    var vProductCode = paramData.productCode;
    var vProductLine = paramData.productLine != null ? paramData.productLine.fullName : "";
    var vProductName = paramData.fullName;
    var vProcessor = paramData.processor.fullName;
    var vProcessorGraphics = paramData.processor.processorGraphics != null && paramData.processor.processorGraphics != "" ? paramData.processor.processorGraphics : "";
    var vGraphics = paramData.graphics != null ? paramData.graphics.fullName : "";

    $(".product-name").html(vProductName);
    $("#product-title").html(vProductName + " (" + vProductCode + ")");
    $("#product-line").html(vProductLine);
    $("#product-price").html(paramData.priceEach != paramData.buyPrice ? "$" + paramData.priceEach.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "<span>$" + paramData.buyPrice.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "</span>" : "$" + paramData.buyPrice.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    $("#li-processor").append(vProcessor);
    $("#li-screen").append(paramData.screenSize + " inch, " + paramData.screenResolutionX + " x " + paramData.screenResolutionY + " pixels" + (paramData.displayType != null && paramData.displayType != "" ? ", " + paramData.displayType : ""));
    $("#li-ram").append(paramData.systemMemory + " GB, " + paramData.typeOfMemory + (paramData.systemMemorySpeed != null ? ", " + paramData.systemMemorySpeed + " MHz" : ""));
    $("#li-graphics").append((paramData.graphics != null ? vGraphics + " " + paramData.graphics.gpuMemory + "GB; " : "") + vProcessorGraphics);
    $("#li-storage").append(paramData.storageType + " " + paramData.totalStorageCapacity + " GB");
    $("#li-os").append(paramData.operatingSystem);
    $("#li-dimension").append(paramData.productWidth + " x " + paramData.productDepth + " x " + paramData.productHeight + " cm, " + paramData.productWeight + " kg");
    $("#li-release").append(paramData.releaseDate);

    var vProductPhotos = paramData.productPhotos;
    if (vProductPhotos != null && vProductPhotos.length > 0) {
        $("#current").prop("src", gBASE_URL + "product-photos/" + vProductPhotos[0].name)
        for (var bI = 0; bI < vProductPhotos.length; bI++) {
            $("#gallery .images").append("<img src='" + gBASE_URL + "product-photos/" + vProductPhotos[bI].name + "' class='img' alt='#'></img>")
        }
    } else {
        $("#current").prop("src", "./assets/images/no-image.png");
    }

    $("#p-description").html(paramData.productDescription);
}

//Hàm xử lý hiển thị chi tiết
function loadViewMoreData(paramData) {
    "use strict";
    $("#p-productCode").html(paramData.productCode);
    $("#p-productName").html(paramData.productName);
    if (paramData.productBrand != null) {
        $("#p-productBrand").html(paramData.productBrand.name);
    } else {
        $("#p-productBrand").html("");
    }
    if (paramData.productLine != null) {
        $("#p-productLine").html(paramData.productLine.name);
    } else {
        $("#p-productLine").html("");
    }
    $("#p-releaseDate").html(paramData.releaseDate);
    $("#p-color").html(paramData.color);
    var vProductPhotos = paramData.productPhotos;
    $("#carousel-images").html("");
    $("#carouselControls").prop("hidden", true);
    if (vProductPhotos != null && vProductPhotos.length > 0) {
        $("#carousel-images").append(
            "<div class='carousel-item active'>"
            + "<img src='" + gBASE_URL + "product-photos/" + vProductPhotos[0].name + "' class='d-block w-100' style='height: 500px; object-fit: contain;' alt='...'>"
            + "</div>"
        );
        for (var bI = 1; bI < vProductPhotos.length; bI++) {
            $("#carousel-images").append(
                "<div class='carousel-item'>"
                + "<img src='" + gBASE_URL + "product-photos/" + vProductPhotos[bI].name + "' class='d-block w-100' style='height: 500px; object-fit: contain;' alt='...'>"
                + "</div>"
            );
        }
        if (vProductPhotos.length > 1) {
            $("#btn-carousel-control-prev").prop("hidden", false);
            $("#btn-carousel-control-next").prop("hidden", false);
        } else {
            $("#btn-carousel-control-prev").prop("hidden", true);
            $("#btn-carousel-control-next").prop("hidden", true);
        }
        $("#carouselControls").prop("hidden", false);
    } else {
        $("#carousel-images").html("");
    }
    var vProcessor = paramData.processor;
    if (vProcessor != null) {
        $("#p-processor").html(vProcessor.fullName);
        $("#p-totalCores").html(vProcessor.totalCores);
        $("#p-totalThreads").html(vProcessor.totalThreads);
        if (vProcessor.baseFrequency != null) {
            $("#p-baseFrequency").html(vProcessor.baseFrequency.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        } else {
            $("#p-baseFrequency").html("");
        }
        if (vProcessor.maxTurboFrequency != null) {
            $("#p-maxTurboFrequency").html(vProcessor.maxTurboFrequency.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        } else {
            $("#p-maxTurboFrequency").html("");
        }
        $("#p-cache").html(vProcessor.cache);
        $("#p-processorGraphics").html(vProcessor.processorGraphics);
        if (vProcessor.graphicsFrequency != null) {
            $("#p-graphicsFrequency").html(vProcessor.graphicsFrequency.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        } else {
            $("#p-graphicsFrequency").html("");
        }
    } else {
        $("#p-processor").html("");
    }
    var vGraphics = paramData.graphics;
    if (vGraphics != null) {
        $("#p-graphics").html(vGraphics.fullName);
        $("#p-gpuMemory").html(vGraphics.gpuMemory);
    } else {
        $("#p-graphics").html("");
    }
    $("#p-storageType").html(paramData.storageType);
    $("#p-totalStorageCapacity").html(paramData.totalStorageCapacity);
    $("#p-ssdType").html(paramData.ssdType);
    $("#p-systemMemory").html(paramData.systemMemory);
    $("#p-typeOfMemory").html(paramData.typeOfMemory);
    $("#p-systemMemorySpeed").html(paramData.systemMemorySpeed);
    $("#p-numberOfMemorySlots").html(paramData.numberOfMemorySlots);
    $("#p-numberOfMemorySticksIncluded").html(paramData.numberOfMemorySticksIncluded);
    $("#p-screenType").html(paramData.screenType);
    $("#p-displayType").html(paramData.displayType);
    $("#p-screenSize").html(paramData.screenSize);
    $("#p-screenResolutionX").html(paramData.screenResolutionX);
    $("#p-screenResolutionY").html(paramData.screenResolutionY);
    $("#p-touchScreen").html(stringYesOrNo(paramData.touchScreen));
    $("#p-wifi").html(paramData.wifi);
    $("#p-bluetooth").html(paramData.bluetooth);
    $("#p-numberOfUSBPorts").html(paramData.numberOfUSBPorts);
    $("#p-headphoneJack").html(stringYesOrNo(paramData.headphoneJack));
    $("#p-productWidth").html(paramData.productWidth);
    $("#p-productHeight").html(paramData.productHeight);
    $("#p-productDepth").html(paramData.productDepth);
    $("#p-productWeight").html(paramData.productWeight);
    $("#p-batteryType").html(paramData.batteryType);
    $("#p-batteryLife").html(paramData.batteryLife);
    $("#p-batteryCells").html(paramData.batteryCells);
    $("#p-powerSupply").html(paramData.powerSupply);
    $("#p-operatingSystem").html(paramData.operatingSystem);
    $("#p-frontFacingCamera").html(stringYesOrNo(paramData.frontFacingCamera));
    $("#p-numericKeyboard").html(stringYesOrNo(paramData.numericKeyboard));
    $("#p-backlitKeyboard").html(stringYesOrNo(paramData.backlitKeyboard));
    $("#p-warranty").html(paramData.warranty);
}

function stringYesOrNo(paramCheck) {
    "use strict";
    if (paramCheck) {
        return "Yes";
    }
    return "No";
}
